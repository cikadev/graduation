# Graduation

A service made for managing the graduation in [President University](https://president.ac.id).

This repository contains the documents for **Graduation** application.

## Services

The project are splitted into multiple services.

- claimer-front-page-server
- graduation-pods
- vending-front-stage-server
- vending-reservation-server
- clearance-admin
- clearance-client
- voice-recorder

## Workflow

[![](graduation-workflow.png)](graduation-workflow.png)

[Draw.io file](https://drive.google.com/open?id=1lRhonOn4EnZZV_xhbdzumMDfZSRKGHG3)

## Volunteer

See [VOLUNTEER.md](VOLUNTEER.md)
